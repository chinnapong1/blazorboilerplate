using AntDesign;
using BoilerplateNet6.Models;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;

namespace BoilerplateNet6.Pages.Account.Center
{
    public partial class Projects
    {
        private readonly ListGridType _listGridType = new ListGridType
        {
            Gutter = 24,
            Column = 4
        };

        [Parameter]
        public IList<ListItemDataType> List { get; set; }
    }
}