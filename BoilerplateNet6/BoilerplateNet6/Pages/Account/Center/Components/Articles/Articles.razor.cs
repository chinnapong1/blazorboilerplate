using BoilerplateNet6.Models;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;

namespace BoilerplateNet6.Pages.Account.Center
{
    public partial class Articles
    {
        [Parameter] public IList<ListItemDataType> List { get; set; }
    }
}