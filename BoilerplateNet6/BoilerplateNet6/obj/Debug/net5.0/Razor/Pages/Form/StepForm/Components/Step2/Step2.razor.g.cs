#pragma checksum "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3d5bb675e52cce3a68a318b85f986f7109f4f5aa"
// <auto-generated/>
#pragma warning disable 1591
namespace BoilerplateNet6.Pages.Form
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.Charts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.ProLayout;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Services;

#line default
#line hidden
#nullable disable
    public partial class Step2 : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __Blazor.BoilerplateNet6.Pages.Form.Step2.TypeInference.CreateForm_0(__builder, 0, 1, 
#nullable restore
#line 3 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                new ColLayoutParam{ Span = 5 }

#line default
#line hidden
#nullable disable
            , 2, 
#nullable restore
#line 4 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                  new ColLayoutParam{ Span = 19 }

#line default
#line hidden
#nullable disable
            , 3, 
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
               FormLayout.Horizontal

#line default
#line hidden
#nullable disable
            , 4, "stepForm", 5, 
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
              _model

#line default
#line hidden
#nullable disable
            , 6, (context) => (__builder2) => {
                __builder2.OpenComponent<AntDesign.Alert>(7);
                __builder2.AddAttribute(8, "Closable", true);
                __builder2.AddAttribute(9, "ShowIcon", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean?>(
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                     true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(10, "Message", "After the transfer is confirmed, the funds will be directly credited to the other party\'s account and cannot be returned.");
                __builder2.AddAttribute(11, "Style", "margin-bottom: 24px;");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(12, "\n    ");
                __builder2.OpenComponent<AntDesign.Descriptions>(13);
                __builder2.AddAttribute(14, "Column", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<OneOf.OneOf<System.Int32, System.Collections.Generic.Dictionary<System.String, System.Int32>>>(
#nullable restore
#line 12 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                            1

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(15, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenComponent<AntDesign.DescriptionsItem>(16);
                    __builder3.AddAttribute(17, "Title", "Account");
                    __builder3.AddAttribute(18, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 13 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
__builder4.AddContent(19, _model.PayAccount);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(20, "\n        ");
                    __builder3.OpenComponent<AntDesign.DescriptionsItem>(21);
                    __builder3.AddAttribute(22, "Title", "Recipient");
                    __builder3.AddAttribute(23, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 14 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
__builder4.AddContent(24, _model.ReceiverAccount);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(25, "\n        ");
                    __builder3.OpenComponent<AntDesign.DescriptionsItem>(26);
                    __builder3.AddAttribute(27, "Title", "Payee Name");
                    __builder3.AddAttribute(28, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 15 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
__builder4.AddContent(29, _model.ReceiverName);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(30, "\n        ");
                    __builder3.OpenComponent<AntDesign.DescriptionsItem>(31);
                    __builder3.AddAttribute(32, "Title", "Amount");
                    __builder3.AddAttribute(33, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __Blazor.BoilerplateNet6.Pages.Form.Step2.TypeInference.CreateStatistic_1(__builder4, 34, 35, 
#nullable restore
#line 17 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                               _model.Amount

#line default
#line hidden
#nullable disable
                        , 36, "Yuan");
                    }
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(37, "\n    ");
                __builder2.OpenComponent<AntDesign.Divider>(38);
                __builder2.AddAttribute(39, "Style", "margin: 24px 0");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(40, "\n    ");
                __builder2.OpenComponent<AntDesign.FormItem>(41);
                __builder2.AddAttribute(42, "Label", "Password");
                __builder2.AddAttribute(43, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __Blazor.BoilerplateNet6.Pages.Form.Step2.TypeInference.CreateInput_2(__builder3, 44, 45, "password", 46, "width: 80%", 47, 
#nullable restore
#line 22 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                                                                context.Password

#line default
#line hidden
#nullable disable
                    , 48, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => context.Password = __value, context.Password)), 49, () => context.Password);
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(50, "\n    ");
                __builder2.OpenComponent<AntDesign.FormItem>(51);
                __builder2.AddAttribute(52, "Style", "margin-bottom: 8px;");
                __builder2.AddAttribute(53, "WrapperCol", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<AntDesign.ColLayoutParam>(
#nullable restore
#line 24 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                                                      _formLayout.WrapperCol

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(54, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenComponent<AntDesign.Button>(55);
                    __builder3.AddAttribute(56, "Type", "primary");
                    __builder3.AddAttribute(57, "OnClick", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 25 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                                        OnValidateForm

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(58, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(59, "Submit");
                    }
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(60, "\n        ");
                    __builder3.OpenComponent<AntDesign.Button>(61);
                    __builder3.AddAttribute(62, "OnClick", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 26 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Form\StepForm\Components\Step2\Step2.razor"
                         Preview

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(63, "Style", "margin-left: 8px;");
                    __builder3.AddAttribute(64, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
                        __builder4.AddContent(65, "Previous Step");
                    }
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            );
        }
        #pragma warning restore 1998
    }
}
namespace __Blazor.BoilerplateNet6.Pages.Form.Step2
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateForm_0<TModel>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::AntDesign.ColLayoutParam __arg0, int __seq1, global::AntDesign.ColLayoutParam __arg1, int __seq2, global::System.String __arg2, int __seq3, global::System.String __arg3, int __seq4, TModel __arg4, int __seq5, global::Microsoft.AspNetCore.Components.RenderFragment<TModel> __arg5)
        {
        __builder.OpenComponent<global::AntDesign.Form<TModel>>(seq);
        __builder.AddAttribute(__seq0, "LabelCol", __arg0);
        __builder.AddAttribute(__seq1, "WrapperCol", __arg1);
        __builder.AddAttribute(__seq2, "Layout", __arg2);
        __builder.AddAttribute(__seq3, "Class", __arg3);
        __builder.AddAttribute(__seq4, "Model", __arg4);
        __builder.AddAttribute(__seq5, "ChildContent", __arg5);
        __builder.CloseComponent();
        }
        public static void CreateStatistic_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, TValue __arg0, int __seq1, global::System.String __arg1)
        {
        __builder.OpenComponent<global::AntDesign.Statistic<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Value", __arg0);
        __builder.AddAttribute(__seq1, "Suffix", __arg1);
        __builder.CloseComponent();
        }
        public static void CreateInput_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::AntDesign.Input<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Type", __arg0);
        __builder.AddAttribute(__seq1, "Style", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
