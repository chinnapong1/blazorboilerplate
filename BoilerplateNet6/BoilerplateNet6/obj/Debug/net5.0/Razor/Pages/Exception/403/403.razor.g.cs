#pragma checksum "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Exception\403\403.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "23597fc9e685b335dd5ba3618f2bf12c171245e1"
// <auto-generated/>
#pragma warning disable 1591
namespace BoilerplateNet6.Pages.Exception
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.Charts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.ProLayout;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Services;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/exception/403")]
    public partial class _403 : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<AntDesign.Result>(0);
            __builder.AddAttribute(1, "Status", "403");
            __builder.AddAttribute(2, "Title", "403");
            __builder.AddAttribute(3, "SubTitle", "Sorry, you are not authorized to access this page.");
            __builder.AddAttribute(4, "Extra", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<AntDesign.Button>(5);
                __builder2.AddAttribute(6, "Type", "primary");
                __builder2.AddAttribute(7, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.AddContent(8, "Back Home");
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
