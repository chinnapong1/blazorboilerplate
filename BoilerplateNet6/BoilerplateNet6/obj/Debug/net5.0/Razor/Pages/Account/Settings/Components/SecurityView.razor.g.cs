#pragma checksum "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1416ae96c00b9d84d73d8c96af50ff1b4cf8d9dd"
// <auto-generated/>
#pragma warning disable 1591
namespace BoilerplateNet6.Pages.Account.Settings
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.Charts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.ProLayout;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Services;

#line default
#line hidden
#nullable disable
    public partial class SecurityView : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<AntDesign.AntList<UserLiteItem>>(0);
            __builder.AddAttribute(1, "ItemLayout", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<AntDesign.ListItemLayout>(
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
                ListItemLayout.Horizontal

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "DataSource", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<UserLiteItem>>(
#nullable restore
#line 6 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
                _data

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<UserLiteItem>)((context) => (__builder2) => {
                __builder2.OpenComponent<AntDesign.ListItem>(4);
                __builder2.AddAttribute(5, "Actions", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.RenderFragment[]>(
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
                       _actions

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(6, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder3) => {
                    __builder3.OpenComponent<AntDesign.ListItemMeta>(7);
                    __builder3.AddAttribute(8, "Avatar", "");
                    __builder3.AddAttribute(9, "Description", global::Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 8 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
                                              context.Description

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(10, "TitleTemplate", (Microsoft.AspNetCore.Components.RenderFragment)((__builder4) => {
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
__builder4.AddContent(11, context.Title);

#line default
#line hidden
#nullable disable
                    }
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 15 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
 
    private static RenderFragment _modify = 

#line default
#line hidden
#nullable disable
        (__builder2) => {
            __builder2.AddMarkupContent(12, "<a key=\"modify\">Modify</a>");
        }
#nullable restore
#line 16 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Account\Settings\Components\SecurityView.razor"
                                                                       ;
    private readonly RenderFragment[] _actions = {_modify};

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
