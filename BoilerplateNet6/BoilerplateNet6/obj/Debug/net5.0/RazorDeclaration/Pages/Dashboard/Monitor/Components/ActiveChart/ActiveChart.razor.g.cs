// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace BoilerplateNet6.Pages.Dashboard.Monitor
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.Charts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using AntDesign.ProLayout;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\_Imports.razor"
using BoilerplateNet6.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Dashboard\Monitor\Components\ActiveChart\ActiveChart.razor"
using BoilerplateNet6.Models;

#line default
#line hidden
#nullable disable
    public partial class ActiveChart : AntDomComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 34 "C:\ISDBoilerplate\BoilerplateNet6\BoilerplateNet6\Pages\Dashboard\Monitor\Components\ActiveChart\ActiveChart.razor"
 
    public ChartDataItem[] ActiveData { get; set; }

    protected override void OnInitialized() {
        base.OnInitializedAsync();
        ActiveData = GetActiveData();
    }

    private ChartDataItem[] GetActiveData() {
        var activeData = new ChartDataItem[24];
        var random = new Random();
        for (var i = 0; i < 24; i++) {
            activeData[i] = new ChartDataItem {
                X = $"{i.ToString().PadRight(2, '0')}: 00",
                Y = (int) (Math.Floor(random.NextDouble() * 200) + i * 50)
            };
        }
        return activeData;
    }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
